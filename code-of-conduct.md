PublicSpaces is een netwerk van publieke organisaties die samen strijden voor een internet gebaseerd op publieke waarden. Wij verwelkomen iedereen die zich hier mee bezig houdt.

# TL;DR
De basis van onze huisregels is dat we ervanuit gaan dat je anderen behandelt zoals jij ook behandeld wilt worden. Met geduld, respect en empathie ongeacht alle verschillen die er tussen mensen kunnen zijn. 
Kort gezegd: be excellent to each other!

Het uitgangspunt binnen alle PublicSpaces projecten, evenementen, bijeenkomsten en (online) ruimtes is dat iedereen zich respectvol, beleefd, collegiaal en solidair gedraagt in de interactie met anderen. 
De veelvormigheid van menselijke diversiteit wordt omarmd in haar al verschijningsvormen. Ongeacht religieuze, etnische en culturele achtergrond, geslacht, geaardheid, leeftijd, sociale klasse, economische status, fysieke en/of mentale mogelijkheden, verschijning etc. 

Van iedereen die meewerkt en/of deelneemt aan PublicSpaces' projecten, evenementen, bijeenkomsten en (online) ruimtes wordt verwacht dat zij op de hoogte zijn van deze huisregels en zich hier ook aan houden.  Deze huisregels zijn dus ook van toepassing op de (online) ruimtes, zoals bijvoorbeeld de mailinglisten, forum, chatrooms en eventuele andere.

# Respectvol communiceren
We verwachten dat iedereen respectvol communiceert. Hieronder verstaan we onder andere: 

- Wees empathisch in je communicatie & gedrag 
Luister en probeer te begrijpen wat anderen met verschillende achtergronden je willen vertellen. Wees bereid om je eigen begrip, verwachtingen en gedrag uit te dagen en aan te passen.  

- Ga uit van goed vertrouwen en goede bedoelingen
Tenzij er hard bewijs is van het tegendeel. Wees constructief en gevoelig bij het geven van kritiek. Sta open voor constructieve kritiek. Help elkaar, waar mogelijk.  

- Respecteer de wijze waarop mensen zichzelf benoemen en beschrijven 
Mensen kunnen specifieke termen gebruiken om zichzelf te beschrijven. Gebruik deze termen, als teken van respect, wanneer je communiceert met of over deze mensen. Etnische groepen kunnen een specifieke naam gebruiken om zichzelf te beschrijven, in plaats van de naam die historisch door anderen werd gebruikt. Mensen kunnen namen hebben die letters, klanken of woorden uit hun taal gebruiken die je misschien niet kent. Mensen die zich identificeren met een bepaalde seksuele geaardheid of genderidentiteit kunnen daarbij aparte namen of voornaamwoorden gebruiken. Mensen met een bepaalde fysieke of mentale beperking kunnen bepaalde termen gebruiken om zichzelf te beschrijven.

# Ongewenst gedrag
We tolereren geen intimidatie of discriminerend gedrag in welke vorm dan ook. Om een beeld te geven van ongewenst gedrag volgt hier een niet uitputtende lijst:  

-  Racistische, homofobe, transfobe, vrouwonvriendelijke of ander uitsluitende vooringenomen of beledigende uitingen dan wel gedrag zijn bij ons niet welkom.

- Val mensen niet lastig. Stalking, fysiek contact zonder toestemming of ongewenste seksuele aandacht zijn intimidatie. Het online volgen van mensen over websites & platformen heen met als doel ze van streek te maken of te ontmoedigen is ook intimidatie. Intimidatie in welke vorm dan ook toleren we niet.   

- Zich op een bepaalde manier kleden of gedragen is geen toestemming.  

- Agressie en arrogantie zijn niet welkom, iedereen moet de ruimte krijgen om vragen te kunnen stellen.  

- Privacy en vertrouwelijkheid zijn belangrijk. Vraag toestemming aan mensen voor je ze filmt, fotografeert of citeert en respecteer hun wens.

- Bedreigingen in welke vorm dan ook zijn onacceptabel. 

# Als de huisregels niet in acht worden genomen?
Mochten er situaties ontstaan waarbij deze huisregels niet in acht worden genomen, dan kun je contact opnemen met Jantien Borsboom - directeur PublicSpaces [TODO: & Bestuurslid] via coc@publicspaces.net [TODO: aanmaken email] Tijdens evenementen kun je ook iemand van de organisatie aanspreken waarna zij je verder kunnen doorverwijzen. PublicSpaces zal dan ingrijpen en passende maatregelen nemen. In uiterste gevallen kan iemand de toegang tot onze evenementen en (online) ruimtes worden ontzegd.     

Heb je op- en/of aanmerkingen op deze huisregels / code of conduct? Neem dan contact op met PublicSpaces via info@publicspaces.net 

_Met dank aan NLUUG, MCH2022 en Mediawiki op basis waarvan wij deze huisregels hebben opgesteld._ 


